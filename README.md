# Comment un système de gestion de versions comme Git améliorera ma productivité ?

## Suivi de mes modifications
Il me permet tout d'abord de suivre les modifications apportées à mon projet. Ce qui est très pratique
pour les projets en équipe car je serai capable de savoir qui a modifié quoi, quand et les raisons des
modifications. 

## Historique des modifications
J'ai également remarqué que Git gardait un historique de mes fichiers. Je peux voir mes fichiers avant
et après modificiations donc je pourrais revenir sur une version antérieure de mon fichier au besoin.

## Gestion de conflits
Dans une des vidéo présentée lors du cours, j'ai également remarqué qu'il me permettrait d'éviter cer-
tains conflits de modifications simultanées de fichiers.

## Branches tests
Par son système de branches, il m'est également possible de créer d'autres branches pour tester mon 
code et s'il est correct je peux le fusionner à la branche principale. Si ce n'est pas le cas, je peux
simplement supprimer la branche créée sans aucune incidence sur mon travail.

